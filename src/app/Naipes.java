package app;

public enum Naipes {
	COPAS("Copas"),
	PAUS("Paus"),
	OUROS("Ouros"),
	ESPADAS("Espadas");
	
	private String descricao;
	
	private Naipes(String descricao) {
		this.descricao = descricao;
	}
	
	public String toString() {
		return descricao;
	}
}
